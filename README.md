# Lua `#inst`

A simple implementation of [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date parser for Lua via patterns, and implementation of the `#inst` tagged literal from Clojure.

## Rationale

Lua doesn't have built-in support for parsing date strings and patterns in Lua are not expressive enough to build a proper ISO 8601 pattern, lacking alterations and optional groups
As a workaround, the library predefines common date and time formats supported by ISO 8601 and composes those into almost all possible variants.
A total of 448 patterns are generated, and tried in order, parsing a given date.

The `inst` function, provided by this library, supports the following formats:

- `YYYY`
- `YYYY-MM`
- `YYYY-MM-DD`
- `YYYY-MM-DDThh:mm:ssZ`
- `YYYY-MM-DDThh:mm:ss.msZ`
- `YYYY-MM-DDThh:mm:ss±hh:mm`
- `YYYY-MM-DDThh:mm:ss.ms±hh:mm`

The dashes and the colon in the zone offset are optional.

### Data literal

The lack of a data literal for dates in Lua can be mitigated by using literal tables, like `{year=2021, day=27, month=8, hour=21, min=48, sec=56}`, but it is harder to read than `"2021-08-27T21:48:56Z"`.
Lua allows function calls without parentheses if there's only one literal argument, so instead it is possible to write `inst "2021-08-27T21:48:56Z"` directly in the code.
To better indicate that this is a data literal, the `__len` and `__tostring` metamethods are redefined for the date object, allowing the following syntax:

``` lua
Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio
> inst = require "io.gitlab.andreyorst.inst"
> #inst "2021"
#inst "2021-01-01T00:00:00.000-00:00"
> #inst "2021-11"
#inst "2021-11-01T00:00:00.000-00:00"
> #inst "2021-11-21"
#inst "2021-11-21T00:00:00.000-00:00"
> #inst "20211121"
#inst "2021-11-21T00:00:00.000-00:00"
> #inst "2021-11-21T23:01:42Z"
#inst "2021-11-21T23:01:42.000-00:00"
> #inst "2021-11-21T23:01:42.228Z"
#inst "2021-11-21T23:01:42.228-00:00"
> #inst "2021-11-21T23:01:42.228+03:00"
#inst "2021-11-21T20:01:42.228-00:00"
> #inst "2021-11-21T23:01:42.228-03:00"
#inst "2021-11-22T02:01:42.228-00:00"
```

Thus, calling `#inst` on a date string returns a representation that can be read back directly by the Lua reader, but still can be used in function calls like `os.time`, thus acting as a data literal:

``` lua
> aorst = { name = "Andrey", birthday = #inst"1992-08-16" }
> aorst.birthday
#inst "1992-08-16T00:00:00.000-00:00"
> os.date("%a %b %d %Y", os.time(aorst.birthday))
Sun Aug 16 1992
```

The `#inst` syntax is inspired by Clojure, in which the `#` followed by identifier is a syntax for tagged data literal, though it is purely superficial in the case of the implementation for the Lua runtime and is used for familiarity:

``` clojure
Clojure 1.11.1
user=> #inst "2024"
#inst "2024-01-01T00:00:00.000-00:00"
user=> #inst "2024-08-25T13:50:42.000-03:00"
#inst "2024-08-25T16:50:42.000-00:00"
```

## Caveats

This library is a proof of concept.
The parser probably doesn't fully implement the ISO 8601 standard.

Use at your own risk.

<!--  LocalWords:  metamethods Lua Lua's predefines -->
