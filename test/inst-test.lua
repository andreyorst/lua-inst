-- Test file. Run with `lua tests.lua` in the root of the project

local inst = require "io.gitlab.andreyorst.inst"

local function date_test(date, date_table)
    local res = os.time(date)
    local expected = os.time(date_table)
    assert(res == expected, ("%s: expected %d, got %d"):format(date_str, expected, res))
end

local function tostring_test(date, expected_str)
    local str = tostring(date)
    assert(str == expected_str, ("%s ~= %s"):format(str, expected_str))
end

-- os.time tests

date_test(#inst"2021",       {year=2021, month=1, day=1, hour=0, min=0, sec=0})
date_test(#inst"2021-02",    {year=2021, month=2, day=1, hour=0, min=0, sec=0})
date_test(#inst"202102",     {year=2021, month=2, day=1, hour=0, min=0, sec=0})
date_test(#inst"2021-02-21", {year=2021, month=2, day=21, hour=0, min=0, sec=0})
date_test(#inst"20210221",   {year=2021, month=2, day=21, hour=0, min=0, sec=0})

date_test(#inst"2021-02-21T23:59:42Z",     {year=2021, month=2, day=21, hour=23, min=59, sec=42})
date_test(#inst"20210221T23:59:42Z",       {year=2021, month=2, day=21, hour=23, min=59, sec=42})
date_test(#inst"2021-02-21T23:59:42.999Z", {year=2021, month=2, day=21, hour=23, min=59, sec=42})
date_test(#inst"20210221T23:59:42.999Z",   {year=2021, month=2, day=21, hour=23, min=59, sec=42})

date_test(#inst"2021-02-21T23:59:42+03:00",     {year=2021, month=2, day=21, hour=20, min=59, sec=42})
date_test(#inst"20210221T23:59:42+03:00",       {year=2021, month=2, day=21, hour=20, min=59, sec=42})
date_test(#inst"2021-02-21T23:59:42.999+03:00", {year=2021, month=2, day=21, hour=20, min=59, sec=42})
date_test(#inst"20210221T23:59:42.999+03:00",   {year=2021, month=2, day=21, hour=20, min=59, sec=42})
date_test(#inst"20240229T23:59:42.999+00:00",   {year=2024, month=2, day=29, hour=23, min=59, sec=42})

date_test(#inst"0000",       {year=0, month=1, day=1, hour=0, min=0, sec=0})
date_test(#inst"0001",       {year=1, month=1, day=1, hour=0, min=0, sec=0})
date_test(#inst"0001-02-03", {year=1, month=2, day=3, hour=0, min=0, sec=0})

date_test(#inst"-2021",       {year=-2021, month=1, day=1, hour=0, min=0, sec=0})
date_test(#inst"-2021-02",    {year=-2021, month=2, day=1, hour=0, min=0, sec=0})
date_test(#inst"-202102",     {year=-2021, month=2, day=1, hour=0, min=0, sec=0})
date_test(#inst"-2021-02-21", {year=-2021, month=2, day=21, hour=0, min=0, sec=0})
date_test(#inst"-20210221",   {year=-2021, month=2, day=21, hour=0, min=0, sec=0})

date_test(#inst"-2021-02-21T23:59:42Z",     {year=-2021, month=2, day=21, hour=23, min=59, sec=42})
date_test(#inst"-20210221T23:59:42Z",       {year=-2021, month=2, day=21, hour=23, min=59, sec=42})
date_test(#inst"-2021-02-21T23:59:42.999Z", {year=-2021, month=2, day=21, hour=23, min=59, sec=42})
date_test(#inst"-20210221T23:59:42.999Z",   {year=-2021, month=2, day=21, hour=23, min=59, sec=42})

date_test(#inst"-2021-02-21T23:59:42+03:00",     {year=-2021, month=2, day=21, hour=20, min=59, sec=42})
date_test(#inst"-20210221T23:59:42+03:00",       {year=-2021, month=2, day=21, hour=20, min=59, sec=42})
date_test(#inst"-2021-02-21T23:59:42.999+03:00", {year=-2021, month=2, day=21, hour=20, min=59, sec=42})
date_test(#inst"-20210221T23:59:42.999+03:00",   {year=-2021, month=2, day=21, hour=20, min=59, sec=42})
date_test(#inst"-20240229T23:59:42.999+00:00",   {year=-2024, month=2, day=29, hour=23, min=59, sec=42})


-- string fromatting

tostring_test(#inst"2021",       "#inst \"2021-01-01T00:00:00.000-00:00\"")
tostring_test(#inst"2021-02",    "#inst \"2021-02-01T00:00:00.000-00:00\"")
tostring_test(#inst"202102",     "#inst \"2021-02-01T00:00:00.000-00:00\"")
tostring_test(#inst"2021-02-21", "#inst \"2021-02-21T00:00:00.000-00:00\"")
tostring_test(#inst"20210221",   "#inst \"2021-02-21T00:00:00.000-00:00\"")

tostring_test(#inst"2021-02-21T23:59:42Z",     "#inst \"2021-02-21T23:59:42.000-00:00\"")
tostring_test(#inst"20210221T23:59:42Z",       "#inst \"2021-02-21T23:59:42.000-00:00\"")
tostring_test(#inst"2021-02-21T23:59:42.999Z", "#inst \"2021-02-21T23:59:42.999-00:00\"")
tostring_test(#inst"20210221T23:59:42.999Z",   "#inst \"2021-02-21T23:59:42.999-00:00\"")

tostring_test(#inst"2021-02-21T23:59:42+03:00",     "#inst \"2021-02-21T20:59:42.000-00:00\"")
tostring_test(#inst"20210221T23:59:42+03:00",       "#inst \"2021-02-21T20:59:42.000-00:00\"")
tostring_test(#inst"2021-02-21T23:59:42.999+03:00", "#inst \"2021-02-21T20:59:42.999-00:00\"")
tostring_test(#inst"20210221T23:59:42.999+03:00",   "#inst \"2021-02-21T20:59:42.999-00:00\"")
tostring_test(#inst"20240229T23:59:42.999+00:00",   "#inst \"2024-02-29T23:59:42.999-00:00\"")

tostring_test(#inst"-2021",       "#inst \"-2021-01-01T00:00:00.000-00:00\"")
tostring_test(#inst"-2021-02",    "#inst \"-2021-02-01T00:00:00.000-00:00\"")
tostring_test(#inst"-202102",     "#inst \"-2021-02-01T00:00:00.000-00:00\"")
tostring_test(#inst"-2021-02-21", "#inst \"-2021-02-21T00:00:00.000-00:00\"")
tostring_test(#inst"-20210221",   "#inst \"-2021-02-21T00:00:00.000-00:00\"")

tostring_test(#inst"-2021-02-21T23:59:42Z",     "#inst \"-2021-02-21T23:59:42.000-00:00\"")
tostring_test(#inst"-20210221T23:59:42Z",       "#inst \"-2021-02-21T23:59:42.000-00:00\"")
tostring_test(#inst"-2021-02-21T23:59:42.999Z", "#inst \"-2021-02-21T23:59:42.999-00:00\"")
tostring_test(#inst"-20210221T23:59:42.999Z",   "#inst \"-2021-02-21T23:59:42.999-00:00\"")

tostring_test(#inst"-2021-02-21T23:59:42+03:00",     "#inst \"-2021-02-21T20:59:42.000-00:00\"")
tostring_test(#inst"-20210221T23:59:42+03:00",       "#inst \"-2021-02-21T20:59:42.000-00:00\"")
tostring_test(#inst"-2021-02-21T23:59:42.999+03:00", "#inst \"-2021-02-21T20:59:42.999-00:00\"")
tostring_test(#inst"-20210221T23:59:42.999+03:00",   "#inst \"-2021-02-21T20:59:42.999-00:00\"")
tostring_test(#inst"-20240229T23:59:42.999+00:00",   "#inst \"-2024-02-29T23:59:42.999-00:00\"")


-- invalid cases
assert(not pcall(inst, "20041330"))
assert(not pcall(inst, "2004-02-30T23:59:42Z"))
assert(not pcall(inst, "2004-02-31T23:59:42Z"))
assert(not pcall(inst, "2004-04-31T23:59:42Z"))
assert(not pcall(inst, "2023-02-29T23:59:42Z"))
assert(not pcall(inst, "20004"))
assert(not pcall(inst, "20004-02-03"))
